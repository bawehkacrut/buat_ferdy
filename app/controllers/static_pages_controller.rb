class StaticPagesController < ApplicationController
  def home
  end

  def about
  end

  def contact
  end

  def gallery
  end

  def product
  end

  def service
  end
end
