Rails.application.routes.draw do

  root 'static_pages#home'
  get '/about',      to: 'static_pages#about'
  get '/contact',    to: 'static_pages#contact'
  get '/gallery',    to: 'static_pages#gallery'
  get '/product',    to: 'static_pages#product'
  get '/service',    to: 'static_pages#service'


  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
