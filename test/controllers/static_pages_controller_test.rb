require 'test_helper'

class StaticPagesControllerTest < ActionDispatch::IntegrationTest
    test "should get home" do
      get root_path
      assert_response :success
      assert_select "title", "Testing Web"
    end


  test "should get about" do
      get about_path
      assert_response :success
      assert_select "title", "About | Testing Web"
  end

  test "should get contact" do
      get contact_path
      assert_response :success
      assert_select "title", "Contact | Testing Web"
  end

  test "should get gallery" do
      get gallery_path
      assert_response :success
      assert_select "title", "Gallery | Testing Web"
  end

  test "should get product" do
      get product_path
      assert_response :success
      assert_select "title", "Product | Testing Web"
  end

  test "should get service" do
      get service_path
      assert_response :success
      assert_select "title", "Service | Testing Web"
  end

end
